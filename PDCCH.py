from bpsk_demod import bpsk_demod
from qpsk_demod import qpsk_demod
from hamming748 import hamming748_decode
from bin2dec import bin2dec

def decode_PDCCHU_stream(mcs, stream) :
    if mcs == 0 :
        return bpsk_demod(stream)
    elif mcs == 2 :
        return qpsk_demod(stream)
    else :
        return -1
def writeDico(PDCCHU_fec):
    pdcchu_dictionnary = {}

    pdcchu_dictionnary['user_ident'] = bin2dec(PDCCHU_fec[0:8])
    pdcchu_dictionnary['mcs'] = bin2dec(PDCCHU_fec[8:14])
    pdcchu_dictionnary['symb_start'] = bin2dec(PDCCHU_fec[14:18])
    pdcchu_dictionnary['RB start'] = bin2dec(PDCCHU_fec[18:24])
    pdcchu_dictionnary['RB size'] = bin2dec(PDCCHU_fec[24:34])
    pdcchu_dictionnary['CRC'] = bin2dec(PDCCHU_fec[35:37])
    
    return pdcchu_dictionnary

def PDDCHCHANNEL(mat,pbchu_dictionnary):
    symb_start = pbchu_dictionnary['symb_start']
    RB = pbchu_dictionnary['RB']
    mcs = pbchu_dictionnary['mcs']
    PDCCHU = mat[symb_start-3][12*(RB-1):12*(RB-1)+72] #-2-1 pour enlever les symboles de synchronisation et on est en python
#print("PDCCHU", PDCCHU)
    PDCCHU_decode = decode_PDCCHU_stream(mcs, PDCCHU)
#print(PDCCHU_decode)
    PDCCHU_fec = hamming748_decode(PDCCHU_decode)
#print(PDCCHU_fec)
    pdcchu_dictionnary = writeDico(PDCCHU_fec)
    return pdcchu_dictionnary