import numpy as np
import matplotlib.pyplot as plt
from bin2dec import bin2dec

from bpsk_demod import bpsk_demod
from hamming748 import hamming748_decode

def RemoveUselessCarrier(TFmatrix, FFT_size):
    """
    Removes the unused subcarriers in the TimeFrequency matrix
    """
    NRe = 624 # Number of allocated subcarriers
    Sa_1 = np.arange(1, NRe//2+1)
    Sa_2 = np.arange(FFT_size-(NRe//2), FFT_size)
    Sa = np.concatenate((Sa_1, Sa_2)) # Vector of allocated subcarriers
    TFmatMask = TFmatrix[:,Sa]
    return TFmatMask

def powerDistributionGraph(Z):
    """
    Draw the power distribution grap
    """
    fig, ax = plt.subplots()
    cs = ax.contourf( np.linspace(0,len(Z), len(Z)),np.linspace(0,len(Z[0]), len(Z[0])), np.transpose(Z))
    ax.set_title('power distribution')
    ax.set_xlabel('symbols')
    ax.set_ylabel('subcarriers')
    plt.plot()

def decodage(mat,good_user_ident,nb_users_int,canalsize,NRe):
    ham_user = []
    user_ident = []
    pbchu_user = []
    i_user_ident = 0
    nb_users_per_canal = NRe//canalsize
    for j in range(0,2) :
        for i in range(1,nb_users_per_canal) :
            if(i-1 + j*nb_users_per_canal <= nb_users_int) :
                pbchu_user.append(bpsk_demod(mat[j][i*canalsize:i*canalsize+canalsize]))
                ham_user.append(hamming748_decode(pbchu_user[i-1+j*(nb_users_per_canal-1)]))
            
                user_ident.append(bin2dec(ham_user[i-1+j*(nb_users_per_canal-1)][0:8]))
                if (user_ident[i-1+j*(nb_users_per_canal-1)] == good_user_ident) :
                    i_user_ident = i-1 + j*(nb_users_per_canal-1)
    return ham_user,user_ident,pbchu_user,i_user_ident

def writeDico(user_ident,i_user_ident,ham_user):
    pbchu_dictionnary = {}
    pbchu_dictionnary['user_ident'] = user_ident[i_user_ident]
    pbchu_dictionnary['mcs'] = bin2dec(ham_user[i_user_ident][8:10])
    pbchu_dictionnary['symb_start'] = bin2dec(ham_user[i_user_ident][10:14])
    pbchu_dictionnary['RB'] = bin2dec(ham_user[i_user_ident][14:20])
    pbchu_dictionnary['HARQ'] = bin2dec(ham_user[i_user_ident][20:24])
    return pbchu_dictionnary

#powerDistributionGraph(mat_complex2)
def PBCHCHANNEL(mat,good_user_ident = 5,NRe = 624):

    #powerDistributionGraph(mat)
        #print(mat[0][:]) #symbole associé à PBCH pour toutes les porteuses

    canalsize = 48
    headpbch = bpsk_demod(mat[0][:canalsize]) #démodulation en bpskdemod

    pbchdecoding = hamming748_decode(headpbch) #PBCH decoding

    #cell ident and nb users
    cell_ident = pbchdecoding[:18]
    nb_users = pbchdecoding[18:24]

    """print("pbchdecoding = ",pbchdecoding)
    print("cell ident = ",cell_ident)
    print("nb users = ", nb_users)"""

    cell_ident_int = bin2dec(cell_ident)
    print("cell ident = ", cell_ident_int)
    nb_users_int = bin2dec(nb_users)
    print("nb users = ", nb_users_int)

    """pbch_channel_size = nb_users_int * canalsize + canalsize
    print("PBCH channel size = ",pbch_channel_size)"""
    ham_user,user_ident,pbchu_user,i_user_ident = decodage(mat,good_user_ident,nb_users_int,canalsize,NRe)
    #print(ham_user)
    #print(user_ident)
    pbchu_dictionnary = writeDico(user_ident,i_user_ident,ham_user)
    return pbchu_dictionnary
