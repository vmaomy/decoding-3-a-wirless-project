from crc import *
from qam16_demod import qam16_demod
from bpsk_demod import bpsk_demod
from qpsk_demod import qpsk_demod
from hamming748 import hamming748_decode
from bin2dec import bin2dec

from binary_transformation import *
import scipy
import math
import sk_dsp_comm.fec_conv as fec

def PBCHU_demod(qamSeq,mcs):
    if mcs == 5 or mcs == 25 : 
        return bpsk_demod(qamSeq)
    elif mcs == 6 or mcs == 26 : 
        return qpsk_demod(qamSeq)
    elif mcs == 7 or mcs == 27 : 
        return qam16_demod(qamSeq)
    #unexpected case
    else :
        return -1
    
def PBCHU_fec(qamSeq,mcs):
    if mcs == 5 or mcs == 6 or mcs == 7 :
        cc1 = fec.FECConv(('1011011','1111001'),6)
        dec = cc1.viterbi_decoder(np.array(qamSeq).astype(int),'hard')
        return dec
        
    elif mcs == 25 or mcs == 26 or mcs == 27 : 
        return hamming748_decode(qamSeq) 
    
    #unexpected case
    else :
        return -1

def PCHU_crc(qamSeq,crcSize):
    poly = get_crc_poly(crcSize)
    return crc_decode(qamSeq,poly)

def nb_point_const(mcs):
    if mcs == 5 or mcs == 25 : 
        return 2
    elif mcs == 6 or mcs == 26 : 
        return 4
    elif mcs == 7 or mcs == 27 : 
        return 16
    #unexpected case
    else :
        return -1

def crc_taille(crc_flag) :
    if crc_flag == 0 :
        return 8
    elif crc_flag == 1 :
        return 16
    elif crc_flag == 2 :
        return 24
    elif crc_flag == 3 :
        return 32
    else :
        return -1

def affiche(payload):
    stringv = ""
    for elt in payload:
        stringv = stringv+elt
    return stringv





'''
q = nb_point_const(mcs)
NFC = int(RB_size * 12 * math.log2(q))
print("NFC = ", NFC)

r = 1/2
N = NFC*r
size_payload = N - crc_size

Z = int(abs(NFC-N)) #nombre de zeros

print("")
'''
def PDSCHCHANNEL(mat, pdcchu_dictionnary):
    Symb_start = pdcchu_dictionnary['symb_start'] 
    RB_start = pdcchu_dictionnary['RB start'] 
    RB_size = pdcchu_dictionnary['RB size'] 
    mcs = pdcchu_dictionnary['mcs']
    crc_flag = pdcchu_dictionnary['CRC']
    user_ident = pdcchu_dictionnary['user_ident']
    crc_size = crc_taille(crc_flag)
    PDSCH_sequence = mat[Symb_start-3][12*(RB_start-1):12*(RB_start-1)+(RB_size)*12] #NFC enlevé

    print("pdsch sequence = ", len(PDSCH_sequence))

    print("pdsh_seqeuence = ", PDSCH_sequence[0:9])

    pbchu_demod = PBCHU_demod(PDSCH_sequence,mcs)

    print("pbchu_demod_size = ", len(pbchu_demod))
    print("pbchu_demod  = ", pbchu_demod[0:9])


    pbchu_fec = PBCHU_fec(pbchu_demod,mcs)

    print("pbchu_fec_size", len(pbchu_fec))

    print("pbchu_fec", pbchu_fec[0:9])

    crc_dec = PCHU_crc(pbchu_fec,crc_size)

    #liste_zeros = [0] * Z
    pbchu_payload = pbchu_fec


    print("payload_size = ",len(pbchu_payload))

    # Assuming the message decoding from QPSK or QAM16 is qamSeq
    # Convert the binary sequence into bytes
    mess = bitToByte(pbchu_payload)
    # Bytes are "encrypted", uncrypt them
    real_mess = cesarDecode(user_ident,mess); # USER is your user group
    final_mess = toASCII(real_mess)
    return affiche(final_mess)










