#We have to control that the parity equations are correct
#Author : Brice JOLIOT
#Date : 29 janvier 2024
from bitFlip import bitFlip
import numpy as np


def hamming748_decode(bitSeq):
    nb_paquets = len(bitSeq)//8

    y = []

    for i in range(0,nb_paquets):
        y = y + hamming748_decode_8bits(bitSeq[i*8:i*8+8])

    return y



def hamming748_decode_8bits(bitSeq_8bits):

    # We define the matrix syndrom H
    H = np.array([[0,0,0,1,1,1,1], [0,1,1,0,0,1,1], [1,0,1,0,1,0,1]])

    y = np.array(bitSeq_8bits[0:7])

    eps = np.dot(H,y)

    eps = eps%2
    
    eps0 = eps[0]
    eps1 = eps[1]
    eps2 = eps[2]


    if (eps0 == 0 and eps1==0 and eps2==0): #no error detected (or the last bit is wrong but syndrome equals to 0)
        return bitSeq_8bits[0:4]


    #error positition
    bit_error = 0
    for i in range(0,len(eps)): 
        bit_error = bit_error + eps[i]*(2**i)
    
    #the python index starts at 0
    bit_error = bit_error - 1

    #Parity calculation
    number_of_1 = 0
    for i in range(0,len(bitSeq_8bits)-1):
        if bitSeq_8bits[i]==1 : 
            number_of_1 = number_of_1 + 1

    parity_bit = bitSeq_8bits[7]
    # 1 : odd
    # 0 : even

    if (parity_bit != (number_of_1%2)): #only one error occurs (parity error and the syndrome is not equal to 0)
        bitSeq_8bits[bit_error] = bitFlip(bitSeq_8bits[bit_error])
        return bitSeq_8bits[0:4]
    
    else : #two errors occur (even number of error and the syndrome is not equal to 0)
        for i in range(0,len(bitSeq_8bits)):
            bitSeq_8bits[i]=0
        return bitSeq_8bits

    