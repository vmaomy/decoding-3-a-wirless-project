import numpy as np
def qpsk_demod(matrice):
    # Need to switch to vector 
    input_sequence = np.matrix.flatten(matrice)
    # Instantiate an empty list
    output_sequence = []
    # Decoding each element 
    for code in input_sequence:
            bit1 = 1
            bit2 = 1
            if np.real(code) < 0:
                bit1 = 0
            if np.imag(code) < 0:
                bit2 = 0
            output_sequence.append(bit1)   
            output_sequence.append(bit2)
    return output_sequence
