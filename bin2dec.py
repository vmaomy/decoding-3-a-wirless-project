def bin2dec(nb):

    n = "0b"
    for b in nb:
        n = n + str(b)
    return int(n, 2)