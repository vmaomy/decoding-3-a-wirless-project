#!/usr/bin/env python3
from PBCH import PBCHCHANNEL,RemoveUselessCarrier
from PDCCH import PDDCHCHANNEL
from PDSCH import PDSCHCHANNEL
import numpy as np
import sys

i = sys.argv[1]
my_data = np.genfromtxt('./files/tfMatrix_'+i+'.csv', delimiter=';')
mat_complex = my_data[:,0::2] +1j*my_data[:,1::2]
mat_complex2 = mat_complex[2:,:]
#print(len(mat_complex2))
#print(len(mat_complex2[0]))
fftsize = 1024
mat = RemoveUselessCarrier(mat_complex2, fftsize)
good_user_ident = 12
NRe = 624

pbchu_dictionnary = PBCHCHANNEL(mat,good_user_ident,NRe)
print("PBCHU of interest = ", pbchu_dictionnary)
pdcchu_dictionnary = PDDCHCHANNEL(mat,pbchu_dictionnary)
print("pdcchu",pdcchu_dictionnary)
final_mess = PDSCHCHANNEL(mat, pdcchu_dictionnary)
print(final_mess)